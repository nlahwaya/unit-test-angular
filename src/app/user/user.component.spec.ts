
import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { UserComponent } from './user.component';
import { UserServiceMock } from '../mocks/user.service.mock';
import { UserService } from '../service/user.service';
  
describe('UserComponent', () => {
    let comp: UserComponent;
    let fixture: ComponentFixture<UserComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                UserComponent
            ], 
            providers: [
                { provide: UserService, useClass: UserServiceMock }
            ]
        }).compileComponents().then(() => { 
            fixture = TestBed.createComponent(UserComponent);
            comp = fixture.componentInstance;  
        }); 
    })); 
 
    it(`should have two users`, () => {  
        expect(comp.users.length).toEqual(2);
    });   
 
    it(`html should render two users`,() => {
        fixture.detectChanges();
        const el = fixture.nativeElement.querySelector('p');
        expect(el.innerText).toContain('nour');
    }); 
});     
