import { Component, OnInit } from '@angular/core';
import { User } from '../model/user';
import { UserService } from '../service/user.service';

@Component({
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent {
  text = 'user page'; 
  users : User[];

  constructor(private userService: UserService) {
    this.users = this.userService.getUsers();
  } 

 
}
  