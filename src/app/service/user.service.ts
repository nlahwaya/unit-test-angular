import { Injectable } from '@angular/core';
import { User } from '../model/user';


@Injectable()
export class UserService {
  constructor() { }

  getUsers(): Array<User> {
      return [ 
          {
              lastName: 'nour',
              firstName: 'lahwaya'
          },
          {
            lastName: 'Mouna', 
            firstName: 'Makni'
          }
      ]; 
  }  
}
